import React from "react";

const Todo = ({ todos, deleteTodo }) => {
  return (
    <>
      {todos.map((todo, i) => {
        return (
          <li key={i}>
            {todo}
            <button onClick={() => deleteTodo(i)}>Delete</button>
          </li>
        );
      })}
    </>
  );
};

export default Todo;
