import React, { useState } from "react";
import Todo from "./components/Todo";

const App = () => {
  const [todo, setTodo] = useState([]);
  const [newTask, setNewTask] = useState("");

  const submitTodo = () => {
    setTodo([...todo, newTask]);
    setNewTask("");
  };

  const deleteTodo = (i) => {
    const updatedTodo = todo.filter((todo, index) => i !== index);
    setTodo([...updatedTodo]);
  };

  return (
    <div>
      <h4>My Todo List</h4>
      <input
        type="text"
        placeholder="todo list...."
        value={newTask}
        onChange={(e) => setNewTask(e.target.value)}
      />
      <button onClick={submitTodo}>Add</button>
      <hr />
      <Todo todos={todo} deleteTodo={deleteTodo}/>
    </div>
  );
};

export default App;
